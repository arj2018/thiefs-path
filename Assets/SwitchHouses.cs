﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchHouses : MonoBehaviour {

    public GameObject HouseGood;
    public GameObject DestroyedHouse;
    public GameObject[] cutscenes;

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Bucket")
        {
            HouseGood.SetActive(false);
            DestroyedHouse.SetActive(true);
            for (int i = 0; i < cutscenes.Length; i++)
            {
                cutscenes[i].SetActive(true);
            }
        }
    }
}
