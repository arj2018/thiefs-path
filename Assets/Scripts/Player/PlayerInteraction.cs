﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour, CameraLook.OnMouseLookListener
{ public static PlayerInteraction instance;

    [SerializeField] private GameObject gun;
    [SerializeField] private RawImage gunAim;
    [SerializeField] private ParticleSystem sparks;
    [SerializeField] private ParticleSystem holes;
    [SerializeField] private float gunForce = 10;
    [SerializeField] private float gunCadency = 0.2f;
    [SerializeField] private float maxDisToInteract = 5;
    [SerializeField] private LayerMask gunTargetMask = -1; //only layer 8
    private Interactable lookingObj;
    private LayerMask interactablesMask = 256; //only layer 8
    private bool isInteracting = false;
    private bool isAiming = false;
    private float cadencyCounter = 0;

    private Animator gunAnim;

    #region Gun Strings
    private string _gunShot = "Shoot";
    private string _gunVisible = "Visible";
    private string _gunWalk = "Walk";
    private string _gunWalkSpeed = "WalkSpeed";
    #endregion

    private void Awake()
    {
        if(instance && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        CameraLook.instance.AddOnMouseLookListener(this);
        Cursor.lockState = CursorLockMode.Locked;

        gunAnim = gun.GetComponent<Animator>();
    }

    private void Update()
    {
        Inputs();
        MovementGun();

        cadencyCounter += Time.deltaTime;
    }

    private void Inputs()
    {
        if (!isAiming)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartInteract();
            }

            if (Input.GetMouseButtonUp(0))
            {
                StopInteract();
            }

            if(Input.GetMouseButtonDown(1) && !isInteracting)
            {
                isAiming = true;
                gunAnim.SetBool(_gunVisible, true);
                gunAim.enabled = true;
            }
        }
        else
        {
            if (Input.GetMouseButtonUp(1))
            {
                isAiming = false;
                gunAim.enabled = false;
                gunAnim.SetBool(_gunVisible, false);
            }

            if (Input.GetMouseButtonDown(0))
            {
                ShotGun();
            }
        }
    }

    public void OnMouseLook()
    {
        if (isInteracting)
            return;

        Ray ray = new Ray(CameraLook.instance.transform.position, CameraLook.instance.transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, maxDisToInteract, interactablesMask))
        {
            Interactable i = hit.transform.GetComponent<Interactable>();
            if (i == lookingObj)
                return;
            else
            {
                if(lookingObj)
                    lookingObj.StopLook();
                lookingObj = i;
                lookingObj.Look();
            }
        }
        else
        {
            if (lookingObj)
            {
                lookingObj.StopLook();
                lookingObj = null;
            }
        }
    }

    public void StopInteract()
    {
        if (lookingObj != null)
        {
            lookingObj.StopInteract();
            isInteracting = false;
        }
    }

    public void StartInteract()
    {
        if (lookingObj != null)
        {
            lookingObj.Interact();
            isInteracting = true;
        }
    }

    public void ShotGun()
    {
        if (cadencyCounter < gunCadency)
            return;

        gunAnim.SetTrigger(_gunShot);

        SoundManager.instance.PlayClip();

        Ray ray = new Ray(CameraLook.instance.transform.position, CameraLook.instance.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 500, gunTargetMask))
        {

            var param = new ParticleSystem.EmitParams();

            sparks.transform.position = hit.point;
            sparks.transform.forward = (hit.normal);
            sparks.Emit(20);
            
            param.rotation3D = Quaternion.LookRotation(hit.normal).eulerAngles;
            param.position = hit.point + hit.normal * 0.01f;
            holes.Emit(param, 1);

            Rigidbody r = hit.transform.GetComponent<Rigidbody>();
            if (r)
            {
                r.AddForceAtPosition(ray.direction * gunForce, hit.point, ForceMode.Force);
            }
        }


        cadencyCounter = 0;
    }

    public void MovementGun()
    {
        if (isAiming)
        {
            gunAnim.SetBool(_gunWalk, Player.instance.IsWalking);
            gunAnim.SetFloat(_gunWalkSpeed, Player.instance.IsRunning ? 2 : 1);
        }
    }
}
