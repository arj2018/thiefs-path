﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour 
{ public static Player instance;
    
    [Header("Walk")]
    [SerializeField] private float walkSpeed = 5;
    [SerializeField] private float runMultiplyer = 2;
    [SerializeField] private float jumpForce = 2;
    [HideInInspector] public bool canWalk = true;
    private Vector3 walkDir;
    private bool isWalking = false;
    private bool isRunning = false;
    private bool isGrounded = true;
    [Space(10)]
    
    [Header("Collider")]
    private Rigidbody rb;
    private CapsuleCollider bodyCollider;

    #region Get&Setter

    public CapsuleCollider BodyCollider
    {
        get
        {
            return bodyCollider;
        }
    }

    public bool IsWalking
    {
        get
        {
            return isWalking;
        }
    }

    public bool IsRunning
    {
        get
        {
            return isRunning;
        }
    }

    public float RunMultiplyer
    {
        get
        {
            return runMultiplyer;
        }
    }

    #endregion

    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        gameObject.layer = 10;
        bodyCollider = GetComponent<CapsuleCollider>();
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
    }

    private void Update()
    {
        //cheat
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        Inputs();
    }

    private void FixedUpdate()
    {
        isGrounded = CheckIsGrounded();
        Walk();
    }

    private void Inputs()
    {
        //walk input
        if (Input.GetKeyDown(KeyCode.LeftShift))
            isRunning = true;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            isRunning = false;

        walkDir = new Vector3(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical"));
        if(walkDir != Vector3.zero && canWalk)
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            Jump();
        }

    }
    
    private void Walk()
    {
        if (!isWalking)
            return;

        Vector3 dir = walkDir;
        float speed = (isRunning) ? runMultiplyer * walkSpeed : walkSpeed;
        dir *= speed * Time.deltaTime;
        Vector3 forward = CameraLook.instance.transform.forward;
        forward.y = 0;
        Vector3 direction = transform.position + (forward * dir.z) + (CameraLook.instance.transform.right * dir.x);
        rb.MovePosition(direction);
    }

    private void Jump()
    {
        Debug.Log("jump");
        rb.AddForce(Vector3.up * jumpForce * 100, ForceMode.Acceleration);
    }

    private bool CheckIsGrounded()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, bodyCollider.height / 1.95f, -1))
        {
            return true;
        }
        else
            return false;
    }

}
