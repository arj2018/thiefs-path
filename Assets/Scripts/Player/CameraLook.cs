﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class CameraLook : MonoBehaviour 
{
    public static CameraLook instance;
    private List<OnMouseLookListener> mouseLookListenerList = new List<OnMouseLookListener>();


    [SerializeField] private float eyesHeight = 1.75f;
    [SerializeField, Range(0.02f, 0.2f)] private float walkLoopIntensity = 0.04f;
    [SerializeField] private float walkLoopSpeed = 11;
    private Vector3 originalCamLocalPos;
    private Camera cam;
    private float walkLoopCount;

    [Header("MouseLook")]
    [SerializeField] private float lookSpeed = 1;
    [SerializeField] private float lookSmoothSpeed = 0.1f;
    [SerializeField] private Vector2 pitchMinMax = new Vector2(-50, 50);
    private Vector3 currentRotation;
    private Vector3 rotationSmoothVelocity;
    private float pitch;
    private float yaw;
    private bool canLook = true;

    [Header("Cutscene")]
    [SerializeField] private Text cutsceneCC_Text;
    
    #region Getter&Setter
    public Animator Anim { get; private set; }
    public Camera GetCamera
    {
        get
        {
            return cam;
        }
    }
    public Text CutsceneCC_Text
    {
        get
        {
            return cutsceneCC_Text;
        }
    }
    public float Pitch
    {
        get
        {
            return pitch;
        }

        set
        {
            if (canLook)
            {
                Debug.LogWarning("can't change camera pitch while the camera is controlled by player");
                return;
            }
            pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);
            pitch = value;
        }
    }
    public float Yaw
    {
        get
        {
            return yaw;
        }

        set
        {
            if (canLook)
            {
                Debug.LogWarning("can't change camera yaw while the camera is controlled by player");
                return;
            }
            yaw = value;
        }
    }
    #endregion


    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start () 
	{
        cam = GetComponent<Camera>();
        Anim = GetComponent<Animator>();

        //set cam settings
        if (cam)
        {
            //set eyes height and cam rotation
            cam.transform.localPosition = Vector3.zero + (Vector3.up * (eyesHeight / 2));
            originalCamLocalPos = cam.transform.localPosition;
            cam.transform.localEulerAngles = Vector3.zero;

            //set collider
            Player.instance.BodyCollider.height = eyesHeight + .1f;
        }


        Quaternion q = new Quaternion(transform.eulerAngles.x, transform.eulerAngles.y, 0, 0);
        instance.Pitch = q.eulerAngles.x;
        yaw = transform.eulerAngles.y;
    }
	
	void Update () 
	{
        WalkUpDownLoop();
    }

    private void LateUpdate()
    {
        Look();
    }

    void WalkUpDownLoop()
    {
        if (Player.instance.IsWalking)
        {
            walkLoopCount += Time.deltaTime * walkLoopSpeed;
            float height = Mathf.Sin(walkLoopCount) * ((Player.instance.IsRunning) ? walkLoopIntensity * Player.instance.RunMultiplyer : walkLoopIntensity);
            cam.transform.localPosition = originalCamLocalPos + Vector3.up * height;
        }
    }

    private void Look()
    {
        if (!canLook)
            return;

        Vector2 mouseMove = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        yaw += Input.GetAxis("Mouse X") * lookSpeed;
        pitch -= Input.GetAxis("Mouse Y") * lookSpeed;
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);
        //currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, lookSmoothSpeed * Time.deltaTime);
        //transform.eulerAngles = currentRotation;
        Quaternion desiredRotation = Quaternion.Euler(new Vector3(pitch, yaw));
        transform.eulerAngles = desiredRotation.eulerAngles;
        if (mouseMove != Vector2.zero)
        {
            OnMouseLook();
        }
    }

    public void DeactivateMovement()
    {
        canLook = false;
    }

    public void ActivateMovement()
    {
        canLook = true;
    }

    #region MouseLookListener
    private void OnMouseLook()
    {
        for (int i = 0; i < mouseLookListenerList.Count; i++)
        {
            mouseLookListenerList[i].OnMouseLook();
        }
    }

    public void AddOnMouseLookListener(OnMouseLookListener listener)
    {
        mouseLookListenerList.Add(listener);
    }

    public interface OnMouseLookListener
    {
        void OnMouseLook();
    }
    #endregion
}
