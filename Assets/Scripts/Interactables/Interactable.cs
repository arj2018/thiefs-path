﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour 
{
    public virtual void Awake()
    {
        ChangeLayerToDontInteract(false);
    }

    public virtual void Interact()
    {

    }

    public virtual void StopInteract()
    {

    }

    public virtual void Look()
    {

    }

    public virtual void StopLook()
    {

    }

    public void ChangeLayerToDontInteract(bool dontInteract)
    {
        if (dontInteract)
        {
            gameObject.layer = 9;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.layer = 9;
            }
        }
        else
        {
            gameObject.layer = 8;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.layer = 8;
            }
        }
    }
}
