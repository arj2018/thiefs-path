﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MoveObj_Interactable : Interactable 
{

    [SerializeField] private float distanceFromCamera = .7f;
    [SerializeField] private float heighRelatedToCam = -0.4f;
    [SerializeField] private float smoothSpeed = .5f;
    [SerializeField] private float throwFForce = 1000;
    [SerializeField] private bool freeRotation = false;
    private bool interacting = false;
    private bool isRotating = false;
    private Rigidbody rb;
    private Color originalColor;
    private MeshRenderer mesh;
    private Vector3 rotationOnGrab;
    private Vector3 rotationOnRotate;
    private float originalHeighRelatedToCam;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        mesh = GetComponentInChildren<MeshRenderer>();
        originalColor = mesh.material.color;
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Update()
    {
        ThrowObj();

        if (interacting)
        {
            if (Input.GetMouseButtonDown(2))
            {
                originalHeighRelatedToCam = heighRelatedToCam;
                heighRelatedToCam = 0;
                isRotating = true;
                freeRotation = true;
                rotationOnRotate = transform.eulerAngles;
                CameraLook.instance.DeactivateMovement();
            }

            if (Input.GetMouseButtonUp(2))
            {
                heighRelatedToCam = originalHeighRelatedToCam;
                isRotating = false;
                freeRotation = false;
                CameraLook.instance.ActivateMovement();
            }

            RotateObj();
        }
    }

    void Move()
    {
        if (!interacting)
            return;

        Vector3 movePos = CameraLook.instance.GetCamera.transform.forward * distanceFromCamera + CameraLook.instance.GetCamera.transform.position + Vector3.up * heighRelatedToCam;
        movePos = Vector3.Lerp(transform.position, movePos, smoothSpeed);
        rb.MovePosition(movePos);
        rb.velocity = Vector3.zero;
        if (!freeRotation)
        {
            rb.angularVelocity = Vector3.zero;
            rb.rotation = Quaternion.Euler(rotationOnGrab);
        }
    }

    void ThrowObj()
    {
        if (interacting && Input.GetMouseButtonDown(1))
        {
            StopInteract();
            PlayerInteraction.instance.StopInteract();
            PlayerInteraction.instance.OnMouseLook();
            rb.AddForce(CameraLook.instance.transform.forward * throwFForce);
        }
    }

    void RotateObj()
    {
        if (!isRotating)
            return;

        Vector3 mouseMove = new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
        rotationOnRotate += mouseMove;
        transform.rotation = Quaternion.Euler(rotationOnRotate);
    }

    public override void Interact()
    {
        interacting = true;
        //rb.isKinematic = true;
        rb.useGravity = false;
        rotationOnGrab = transform.eulerAngles;
        ChangeLayerToDontInteract(true);
    }

    public override void StopInteract()
    {
        interacting = false;
        //rb.isKinematic = false;
        rb.useGravity = true;
        ChangeLayerToDontInteract(false);
        
        if(isRotating)
        {
            isRotating = false;
            CameraLook.instance.ActivateMovement();
        }
    }

    public override void Look()
    {
        mesh.material.color = Color.red;
    }

    public override void StopLook()
    {
        mesh.material.color = originalColor;
    }
}
