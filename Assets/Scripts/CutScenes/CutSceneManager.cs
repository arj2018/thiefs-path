﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneManager : MonoBehaviour 
{
    public static CutSceneManager instance;

    [SerializeField] private KeyCode keyToPassText = KeyCode.Mouse0;
    [SerializeField] private float smoothLook = 0.4f;
    public static bool IsPlaying { get; private set; }
    public static Cutscene AtualCutscene { get; private set; }
    public static int CC_Index { get; private set; }

    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(this);
            Debug.LogWarning("Multiple CutsceneManager in the scene");
        }
        else
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (!IsPlaying)
            return;

        LookToCutsceneTarget();
        if (Input.GetKeyDown(keyToPassText))
        {
            NextCC();
        }

    }

    private static void LookToCutsceneTarget()
    {
        Vector3 direction = (AtualCutscene.LookPosition + AtualCutscene.transform.position) - CameraLook.instance.transform.position;

        Quaternion desiredRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(CameraLook.instance.transform.rotation, desiredRotation, instance.smoothLook * Time.deltaTime).eulerAngles;
        rotation.z = 0;
        CameraLook.instance.transform.eulerAngles = rotation;

        //keep camera rotation (pitch & yaw) the same as the animation
        Quaternion q = new Quaternion(rotation.x,rotation.y,0,0);
        CameraLook.instance.Pitch = q.eulerAngles.x;
        CameraLook.instance.Yaw = rotation.y;
    }

    public static void StartCutscene(Cutscene cutscene)
    {
        if (!CameraLook.instance || IsPlaying)
            return;

        CameraLook.instance.Anim.SetTrigger("StartCutscene");
        CameraLook.instance.DeactivateMovement();
        IsPlaying = true;
        AtualCutscene = cutscene;
        CC_Index = 0;
        CameraLook.instance.CutsceneCC_Text.text = AtualCutscene.CC_Text[CC_Index];
        Player.instance.canWalk = false;
    }

    public static void EndCutscene()
    {
        if (!CameraLook.instance || !IsPlaying)
            return;

        CameraLook.instance.Anim.SetTrigger("EndCutscene");
        CameraLook.instance.ActivateMovement();
        IsPlaying = false;
        Player.instance.canWalk = true;
    }

    public static void NextCC()
    {
        if (!CameraLook.instance || !IsPlaying)
            return;

        CC_Index++;

        if (CC_Index >= AtualCutscene.CC_Text.Length)
        {
            EndCutscene();
        }
        else
        {
            CameraLook.instance.CutsceneCC_Text.text = AtualCutscene.CC_Text[CC_Index];
        }
    }
}
