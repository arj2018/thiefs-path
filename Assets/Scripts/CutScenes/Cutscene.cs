﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class Cutscene : MonoBehaviour 
{
    [SerializeField] private string CutsceneName = "Default";
    [SerializeField] private Vector3 lookPosition;
    [SerializeField] private bool oneTimeCutscene = true;
    [SerializeField] private string[] cC_Text = { "CC Example", "Other text" };
    private SphereCollider sCollider;

    public bool AlreadyPlayed { get; private set; }

    public string[] CC_Text
    {
        get
        {
            return cC_Text;
        }
    }
    public Vector3 LookPosition
    {
        get
        {
            return lookPosition;
        }
    }


    void Start () 
	{
        sCollider = GetComponent<SphereCollider>();
        sCollider.isTrigger = true;
        gameObject.layer = 11; //cutscene layer
    }

    private void StartCutscene()
    {
        CutSceneManager.StartCutscene(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (oneTimeCutscene && AlreadyPlayed)
            return;

        Debug.Log("StartCutscene - " + CutsceneName);
        StartCutscene();
        AlreadyPlayed = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Vector4(1,0,0,0.5f); //red with half alpha
        Gizmos.DrawSphere(transform.position + lookPosition, 1);
    }
}
