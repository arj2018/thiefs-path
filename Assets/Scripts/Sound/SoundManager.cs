﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class SoundManager : MonoBehaviour 
{
    public static SoundManager instance;

    private void Awake()
    {
        if(instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public AudioClip clip;
    private List<AudioSource> sources = new List<AudioSource>();

    public void PlayClip()
    {
        AudioSource s = gameObject.AddComponent<AudioSource>();
        sources.Add(s);
        s.playOnAwake = false;
        s.clip = clip;
        s.loop = false;
        s.time = 0.1f;
        s.Play();
        StartCoroutine(DestroySourceAfterEnd(s));
    }

    IEnumerator DestroySourceAfterEnd(AudioSource source)
    {
        yield return new WaitUntil(() => !source.isPlaying);
        sources.Remove(source);
        Destroy(source);
    }

}
